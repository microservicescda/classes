package com.classes.dao;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document("classe")
@AllArgsConstructor
@NoArgsConstructor
public class ClasseDao {
    @Id
    String id;
    String nom;
    List<String> professeurs;
    List<String> eleves;
}
