package com.classes.repository;

import com.classes.dao.ClasseDao;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClasseRepository extends MongoRepository<ClasseDao, String> {
    ClasseDao findByNom(String nom);
}
