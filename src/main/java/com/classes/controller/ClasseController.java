package com.classes.controller;

import com.classes.dao.ClasseDao;
import com.classes.service.ClasseService;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("classes")
public class ClasseController {

    private ClasseService service;
    ClasseController(ClasseService service) {
        this.service = service;
    }

    @GetMapping()
    public List<ClasseDao> findAll() {
        return service.findAll();
    }

    @GetMapping("{id}")
    public ClasseDao findById(@PathVariable String id) {
        return service.findById(id);
    }

    @GetMapping("name/{nom}")
    public ClasseDao findByName(@PathVariable String nom) {
        return service.findByName(nom);
    }

    @GetMapping("all/ids")
    public List<ClasseDao> findAllById(@RequestParam String ids) {
        List<String> arr = new ArrayList<>(Arrays.asList(ids.split(",")));
        return service.findAllById(arr);
    }

    @PostMapping()
    public ClasseDao save(@RequestBody ClasseDao entity) {
        return service.save(entity);
    }

    @DeleteMapping("{id}")
    public void deleteById(@PathVariable String id) {
        service.deleteById(id);
    }
}
