package com.classes.service;

import com.classes.dao.ClasseDao;
import com.classes.repository.ClasseRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class ClasseService {

    private ClasseRepository repo;

    ClasseService(ClasseRepository repository) {
        this.repo = repository;
    }

    public List<ClasseDao> findAll() {
        return repo.findAll();
    }

    public ClasseDao findByName(String nom) {
        return repo.findByNom(nom);
    }

    public List<ClasseDao> findAllById(List<String> ids) {
        return repo.findAllById(ids);
    }

    public ClasseDao save(ClasseDao entity) {
        return repo.save(entity);
    }

    public ClasseDao findById(String id) {
        return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public void deleteById(String id) {
        repo.deleteById(id);
    }
}
